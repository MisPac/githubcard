import React, {Component} from 'react';
import './App.css';
import CardImport from "./cards.js" 

const githubUserAccountInfo = "https://api.github.com/users/davegregg";
class App extends Component {
  state = { user: {}, active: false }


  handleClick = event => {
    if (this.state.active === false) {
      fetch(githubUserAccountInfo)
        .then(dataresponsething => dataresponsething.json())
        .then(takemydata => { 
          console.log(takemydata)
          this.setState({ user: takemydata, active: true })
      })
      
    }
    else {
      this.setState({ active: false })
    }
  }

  render() {
    if (this.state.active === false) {
      return <button onClick={this.handleClick}>
        CLick Me!
   </button>
    }
    else {
   return ( 
   <React.Fragment>
      <button onClick={this.handleClick}>
          CLick Me!
    </button>
    <CardImport 
    src={this.state.user.avatar_url}
    alt={this.state.user.name}
    name={this.state.user.name}
    info={this.state.user.bio}
    evenMoreInfo={this.state.user.followers}
    /> 
  </React.Fragment>
   )
    }
  }
}

export default App;














