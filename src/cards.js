import React from "react"
import { Card, Image, Icon } from 'semantic-ui-react'

const CardImport = (prop) => (
    <Card>
      <Image src= {prop.src} alt= {prop.alt} wrapped ui={false} />
      <Card.Content>
        <Card.Header>{prop.name}</Card.Header>
        <Card.Meta>
          <span className='date'>Works at kenzie</span>
        </Card.Meta>
        <Card.Description>
          {prop.info}
          {prop.evenMoreInfo}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
          <Icon name='user' />
          22 Friends
      </Card.Content>
    </Card>
  )
  
export default CardImport





